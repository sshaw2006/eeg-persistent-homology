from __future__ import print_function
import cPickle as pickle
import pyximport; pyximport.install()
from cython_helpers import filtrations, partial_cor
import networkx as nx
import Holes as ho
import os
from os.path import dirname, abspath
from scipy.io import loadmat
import pandas as pd
from os.path import join


def create_edges(eeg_dataset_path, data_path, seconds, hz=500):
    """
    Running this method requires you to make a folder with the following structure in /data.
    
    /data
        /time_of_{}_s # Where {} is the seconds variable
            /edge_vals
            /gen
            /clique_dictionaries
    
    :param eeg_dataset_path: 
    :param data_path: 
    :param seconds: 
    :param hz: 
    :return: 
    """
    mat = loadmat(eeg_dataset_path)  # load mat-file
    mdata = mat['EEGdata']  # variable in mat file

    df = pd.DataFrame(mdata)
    data = df.as_matrix()
    m, n = data.shape

    # 1s = 500 timesteps (samples at 500 Hz)
    timesteps = int(hz*seconds)

    start_idx = 0
    end_idx = 0
    time_of_pth = join(data_path, 'time_of_{0}_s'.format(seconds))
    edge_folder = join(time_of_pth, 'edge_vals')

    counter = 0
    while end_idx < n:
        end_idx = start_idx + timesteps if start_idx + timesteps <= n else n
        data_slice = data[:, start_idx: end_idx]
        start_idx = end_idx
        pcor_slice = partial_cor.partial_corr(data_slice)
        edge_path = join(edge_folder, '_{0}.txt'.format(counter))

        with open(edge_path, 'w') as f:
            _m, _n = pcor_slice.shape
            for i in range(_m):
                for j in range(i + 1):
                    f.write('{0} {1} {2}\n'.format(str(float(i)), str(float(j)), str(pcor_slice[i, j])))
        counter += 1


def weight_rank_filter(edge_file, save_file):
    G = nx.read_weighted_edgelist(edge_file)
    fil = filtrations.dense_graph_weight_clique_rank_filtration(G, 1)
    with open(save_file, 'wb') as f:
        pickle.dump(fil, f)
    return save_file


def calculate_persistent_homology(edge_dir, output_dir, hom_dim=1):
    import time
    t1= time.time()
    fls = os.listdir(edge_dir)
    kv_files = [(i, i.split('_')[-1].split('.')[0]) for i in fls]
    edge_paths = [edge_dir + '/{0}'.format(i[0]) for i in sorted(kv_files, key=lambda line: int(line[1]))]

    for ep in edge_paths:
        dataset_tag = ep.split('/_')[-1].split('.txt')[0]
        clique_save_file = os.path.join(output_dir, 'clique_dictionaries/_{}.pkl'.format(dataset_tag))
        fil_file = weight_rank_filter(ep, clique_save_file)
        ho.persistent_homology_calculation(fil_file, hom_dim, dataset_tag, output_dir + '/', m1=512, m2=2048)

    dt = time.time() - t1
    print("######################")
    print("Total time taken is: {}".format(dt))
    print("######################")

if __name__ == '__main__':
    repo_dir = dirname(abspath(__file__))
    print(repo_dir)
    data_dir = join(repo_dir, 'data')
    print(data_dir)

    create_edges(join(repo_dir, 'EEG_test_data_vect_20000.mat'), data_dir, seconds=0.01)

    fls = [os.path.join(data_dir, i) for i in os.listdir(data_dir) if 'time_of' in i]
    for f in fls:
        edge_folder = os.path.join(f, 'edge_vals')
        print("edge folder is:{}".format(edge_folder))
        print('f is:{}'.format(f))
        calculate_persistent_homology(edge_folder, f, hom_dim=1)
